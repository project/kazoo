<?php
/**
 * @file
 * Admin settings form and related admin-only functions.
 */


/**
 * Form builder:  The admin settings for API authentication credentials.
 */
function kazoo_admin_form($form, &$form_state) {
  // Check the credentials if this form is being build for the first time.
  if (empty($form_state['input'])) {  
    if (kazoo_check_credentials()) {
      drupal_set_message(t('The current credentials are valid.'), 'status', FALSE);
    }
    else {
      drupal_set_message(t('The current credentials are not valid'), 'error', FALSE);
    }
  }

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Kazoo API base URL'),
    '#description' => t('This should include the protocol, port and version.  Exclude the trailing slash.  E.g. !example', array('!example' => "<code>http://api.2600hz.com:8000/v1</code>")),
    '#required' => TRUE,
    '#default_value' => kazoo_var('access_point'),
  );

  $hash = (Boolean) kazoo_var('hash');

  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credentials'),
    '#description' => t('Leave blank to use the existing credentials.'),
    '#collapsible' => $hash,
    '#collapsed' => TRUE,
  );

  $form['credentials']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#description' => t('The username of the user Drupal should use to authenticate with the Kazoo API.'),
    '#required' => !$hash,
  );

  $form['credentials']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => !$hash,
  );

  $form['credentials']['kazoo_api_account_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account name'),
    '#default_value' => kazoo_var('account_name'),
  );

  return system_settings_form($form);
}

/**
 * Form validate:  Validates API authentication credentials.
 *
 * Checks the API authentication credentials and stores a hash of the username
 * and password, as well as the access point URL, authentication token and
 * account ID.
 */
function kazoo_admin_form_validate($form, &$form_state) {
  $values = &$form_state['values'];

  // Get the MD5 hash, but do not store the raw username and password.
  $name = $values['username'];
  $pass = $values['password'];
  unset($values['username']);
  unset($values['password']);
  $hash = (empty($name) && empty($pass)) ? kazoo_var('hash') : md5("$name:$pass");

  // Check the credentials if there is at least a hash and a URL.
  if (!empty($values['url']) && !empty($hash)) {
    $url = trim($values['url'], '/');
    $query = new kazoo_api_auth($hash, $values['kazoo_api_account_name'], $url);
    $query->execute();
    // $query->debug();

    if (!$query->code) {
      form_set_error('url', t('Invalid address in the URL.'));
    }
    else if ($query->error || empty($query->result->auth_token)) {
      form_set_error('username', t('Drupal could not authenticate with the Kazoo API using these credentials.'));
      form_set_error('password');
      form_set_error('kazoo_api_account_name');
    }
    else {
      // Set the variables that are to be saved tot he database.
      $values['kazoo_api_hash'] = $hash;
      $values['kazoo_api_token'] = $query->result->auth_token;
      $values['kazoo_api_access_point'] = $url;
      $values['kazoo_api_account_id'] = $query->result->data->account_id;
    }
  }
}
