<?php
/**
 * @file
 * Kazoo API Query class.
 */


// A base class for Kazoo API query classes.
abstract class kazoo_api_base extends rest_api_query {
  // Kazoo API expects an accept header.
  public $headers = array(
    'Content-type' => 'application/json',
    'Accept' => 'application/json',
  );

  // Kazoo API does not support file extensions, e.g. ".json".
  public $format = '';

  function __construct($url = NULL) {
    $this->access_point = $url ? $url : kazoo_var('access_point');
    $this->initialized = TRUE;
  }

  function execute() {
    // Kazoo API expects all data parameters to be on the "data" property.
    $this->data = array('data' => $this->data);
    return parent::execute();
  }
}

// A query class just for checking credentials and/or getting a new Auth token.
class kazoo_api_auth extends kazoo_api_base {
  public $method = 'PUT';
  public $resource = 'user_auth';

  function __construct($hash = NULL, $account_name = NULL, $url = NULL) {
    $this->data['credentials'] = $hash ? $hash : kazoo_var('hash');
    $this->data['account_name'] = $account_name ? $account_name : kazoo_var('account_name');
    parent::__construct($url);
  }
}

// A query class for most queries.
class kazoo_api_query extends kazoo_api_base {
  // Most API endpoints use the accounts resource.
  public $resource = 'accounts';

  function __construct() {
    // Most queries need the Auth Token header and the account ID.
    $this->headers['X-Auth-Token'] = kazoo_var('token');
    $this->identifier = kazoo_var('account_id');
    parent::__construct();
  }

  function execute() {
    $return = parent::execute();

    // @todo Test for expired token and renew with new kazoo_api_auth
    // if ($this->success) {
    //   variable_set('kazoo_api_token', $this->auth_token);
    //   variable_set('kazoo_api_account_id', $this->result->account_id);
    // }

    // Set the object's own success, error, code and result properties from the
    // Kazoo API's response data.
    if ($this->result->status !== 'success') {
      $this->success = FALSE;
      $this->error = $this->result->message;
      $this->code = $this->result->error;
    }
    $this->result = $this->result->data;

    return $return;
  }
}
